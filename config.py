from utils import database
node_list = [("n061", "20191125"), ("n062", "20191126")]
def preconfig():
    db = database.db_connec()
    database.create_node_group(db, node_list)
    database.create_abnormality_table(db)
    for node in node_list:
        database.create_node_table(db, node[0])
    db.commit()
    db.close()
preconfig()