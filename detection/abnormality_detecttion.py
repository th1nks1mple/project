import os
from datetime import datetime
import itertools
import math
import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.tsa.api as smt
from pyramid.arima import auto_arima
from sklearn.metrics import mean_squared_error


data1 = {'20191120': 518.0, '20191121': 457.0, 
        '20191122': 448.0, '20191123': 455.0, 
        '20191124': 502.0, '20191125': 460.0, 
        '20191126': 510.0, '20191127': 309.0, 
        '20191128': 453.0, '20191129': 501.0, 
        '20191130': 443.0, '20191201': 486.0,
        '20191202': 447.0, '20191203': 454.0, 
        '20191204': 503.0, '20191205': 461.0, 
        '20191206': 508.0, '20191207': 307.0, 
        '20191208': 456.0, '20191209': 504.0, 
        '20191210': 441.0, '20191211': 485.0,  
        '20191212': 490.0,}

data2 = {'20191121': 2.0, '20191122': 2.0, 
        '20191125': 7.0, '20191126': 2.0, 
        '20191128': 2.0, '20191202': 2.0}

def preprocess(data,datestr):
    datelist = []
    for key in data:
        datelist.append(datetime.strptime(key, '%Y%m%d'))
    datelist = pd.date_range(start=min(datelist), end=datetime.strptime(datestr,'%Y%m%d'))
    newdata = {}
    for date in datelist:
        newdata[datetime.strftime(date,'%Y%m%d')] = 1.0
    for key in newdata:
        if key in data.keys():
            newdata[key] = data[key]
    return newdata

def detector(data,datestr):
    data = preprocess(data, datestr)
    if(len(data) <= 21):
        return False
    ## process pre data
    datelist = [key for key in data]
    valuelist = [value for value in data.values()]

    data_df = pd.DataFrame({'load_date': datelist, 'actuals': valuelist})
    # print(data_df)
    ## Extract the value and apply log transform 
    actual_vals = data_df.actuals.values

    ## Preprocess data for sarima
    train, test = actual_vals[0:-1], actual_vals[-1:]
    train_log, test_log = np.log10(train), np.log10(test)
    ## Auto generate parameter p,d,q for sarima
    stepwise_model = auto_arima(train_log, start_p=1, start_q=1,
                            max_p=3, max_q=3, m=7,
                            start_P=0, seasonal=True,
                            d=1, D=1, trace=True,
                            error_action='ignore',  
                            suppress_warnings=True, 
                            stepwise=True)
    ## SARIMA process
    history = [x for x in train_log]

    stepwise_model.fit(history)
    output = stepwise_model.predict(n_periods=1)
    predict_log = output[0]
    predict = 10**predict_log
    history.append(test[0])
    # print(predict)

    predicted_df = pd.DataFrame()
    predicted_df['load_date'] = data_df['load_date'][-1:]
    predicted_df['actuals']   = test
    predicted_df['predicted'] = predict
    predicted_df.reset_index(inplace=True)
    del predicted_df['index']

    ## detect classify anomalies

    # window = 7 # a week
    # predicted_df.replace([np.inf, -np.inf], np.NaN, inplace=True)
    # predicted_df.fillna(0, inplace=True)
    # predicted_df['percentage_change'] = ((predicted_df['actuals'] - predicted_df['predicted']) / predicted_df['actuals']) * 100
    # predicted_df['error']=predicted_df['actuals']-predicted_df['predicted']
    # ## Moving Average
    # predicted_df['meanval'] = predicted_df['error'].rolling(window=window).mean()
    # predicted_df['deviation'] = predicted_df['error'].rolling(window=window).std()
    # predicted_df['-1s'] = predicted_df['meanval'] - (2 * predicted_df['deviation'])
    # predicted_df['1s'] = predicted_df['meanval'] + (2 * predicted_df['deviation'])
    # cut_list = predicted_df[['error','-1s', 'meanval', '1s']]
    # cut_values = cut_list.values
    # cut_sort = np.sort(cut_values)
    # predicted_df['impact'] = [(lambda x: np.where(cut_sort == predicted_df['error'][x])[1][0])(x) for x in
    #                         range(len(predicted_df['error']))]
    # severity = {0:1, 1:0, 2:0, 3:1}
    # region = {0:"NEGATIVE",1:"NEGATIVE",2:"POSITIVE",3:"POSITIVE"}
    # predicted_df['color'] =  predicted_df['impact'].map(severity)
    # predicted_df['region'] = predicted_df['impact'].map(region)
    # predicted_df['anomaly_points'] = np.where(predicted_df['color'] == 1, predicted_df['error'], np.nan)
    # print(predicted_df)
    # # print(predicted_df['color'].tolist())
    # if (predicted_df['color'].tolist()[0] == 1):
    #     return True
    # return False

    predicted_df['percentage_change'] = ((predicted_df['actuals'] - predicted_df['predicted']) / predicted_df['actuals']) * 100
    if(abs(predicted_df['percentage_change'].tolist()[0]) >= 500):
        return True
    return False

# print(detector(data1, "20191202"))
# print(detector(data2, "20191202"))