import itertools
import math
import warnings
warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import statsmodels.api as sm
import statsmodels.tsa.api as smt
from pyramid.arima import auto_arima
from sklearn.metrics import mean_squared_error

from plotly.offline import (download_plotlyjs, 
        init_notebook_mode, 
        plot, iplot)
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.tools as tls
from matplotlib import pyplot
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')

## generate data

# datelist = pd.date_range(end=pd.datetime.today(), periods=100).tolist()
# valuelist = []
# for _ in range(100):
#     value = np.random.randint(100, 1500)
#     valuelist.append(value)

# data_df = pd.DataFrame({'load_date': datelist, 'actuals': valuelist})
data = sm.datasets.co2.load_pandas()
data_df = data.data

data_df = data_df['co2'].resample('MS').mean()
data_df = data_df.fillna(data_df.bfill())

## Extract the value and apply log transform 
actual_vals = data_df.tolist()
actual_log = np.log10(actual_vals)

## Preprocess data for sarima
train, test = actual_vals[0:-36], actual_vals[-36:]

train_log, test_log = np.log10(train), np.log10(test)

## SARIMA process
history = [x for x in train_log]
predictions = list()
predict_log = list()

my_order = (1, 1, 1)
my_seasonal_order = (1, 1, 1, 12)

for t in range(len(test_log)):
    model = sm.tsa.SARIMAX(history, 
            order=my_order, 
            seasonal_order=my_seasonal_order,
            enforce_stationarity=False,
            enforce_invertibility=False)
    model_fit = model.fit(disp=0)
    output = model_fit.forecast()
    predict_log.append(output[0])
    yhat = 10**output[0]
    predictions.append(yhat)
    obs = test_log[t]
    history.append(obs)
    # print('predicted=%f, expected=%f' % (output[0], obs))
#error = math.sqrt(mean_squared_error(test_log, predict_log))
#print('Test rmse: %.3f' % error)
## plot
figsize=(12, 7)
plt.figure(figsize=figsize)
pyplot.plot(test,label='Actuals')
pyplot.plot(predictions, color='red',label='Predicted')
pyplot.legend(loc='upper right')
pyplot.show()