import os

project_dir = "/home/thinksimple/Workspace/MyProject/"

GRAPH_BASIC_WIDTH = 1
GRAPH_DEGREE_RATIO = 0.002
GRAPH_FONT_RATIO = 7
GRAPH_FONT_COLOR = 'black'
GRAPH_LINE_TYPE = 'bold'

GRAPH_NODE_LEVEL0_LIMIT = 0
GRAPH_NODE_LEVEL1_LIMIT = 200
GRAPH_NODE_LEVEL2_LIMIT = 500

GRAPH_EDGE_LEVEL0_LIMIT = 0
GRAPH_EDGE_LEVEL1_LIMIT = 5
GRAPH_EDGE_LEVEL2_LIMIT = 100
GRAPH_EDGE_LEVEL3_LIMIT = 900

GRAPH_COLOR_LEVEL0 = 'gray'
GRAPH_COLOR_LEVEL1 = 'lightskyblue'
GRAPH_COLOR_LEVEL2 = 'tan1'
GRAPH_COLOR_LEVEL3 = 'crimson'

# Host sending and receiving ARP frame
class Node:
    mac = None
    degree = 0
    def __init__(self,macstr,degree):
        self.mac = macstr
        self.degree = degree
    def get_macstr(self):
        return str(self.mac)

# ARP Request
class RequestEdge:
    src_mac = None
    dst_mac = None
    num = 0
    def __init__(self,src_macstr,dst_macstr,num):
        self.src_mac = src_macstr
        self.dst_mac = dst_macstr
        self.num = num
    def get_src_macstr(self):
        return str(self.src_mac)
    def get_dst_macstr(self):
        return str(self.dst_mac)

# Host for visulization
class VisualNode:
    node = None
    width = 0
    color = ''
    fontsize = 0
    def __init__(self,node,node_num):
        self.node = node
        self.width = GRAPH_BASIC_WIDTH+(node.degree*GRAPH_DEGREE_RATIO)/node_num
        #self.width = GRAPH_BASIC_WIDTH+(node.degree)/node_num
        self.fontsize = self.width*GRAPH_FONT_RATIO
        if node.degree > GRAPH_NODE_LEVEL2_LIMIT:
            self.color = GRAPH_COLOR_LEVEL3
        elif node.degree <= GRAPH_NODE_LEVEL2_LIMIT and node.degree > GRAPH_NODE_LEVEL1_LIMIT:
            self.color = GRAPH_COLOR_LEVEL2
        elif node.degree <= GRAPH_NODE_LEVEL1_LIMIT and node.degree > GRAPH_NODE_LEVEL0_LIMIT:
            self.color = GRAPH_COLOR_LEVEL1
        else:
            self.color = GRAPH_COLOR_LEVEL0

# Edge for visulization
class VisualEdge:
    edge = None
    color = ''
    def __init__(self,edge):
        self.edge = edge
        if edge.num > GRAPH_EDGE_LEVEL3_LIMIT:
            self.color = GRAPH_COLOR_LEVEL3
        elif edge.num > GRAPH_EDGE_LEVEL2_LIMIT and edge.num <= GRAPH_EDGE_LEVEL3_LIMIT:
            self.color = GRAPH_COLOR_LEVEL2
        elif edge.num > GRAPH_EDGE_LEVEL1_LIMIT and edge.num <= GRAPH_EDGE_LEVEL2_LIMIT:
            self.color = GRAPH_COLOR_LEVEL1
        elif edge.num > GRAPH_EDGE_LEVEL0_LIMIT and edge.num <= GRAPH_EDGE_LEVEL1_LIMIT:
            self.color = GRAPH_COLOR_LEVEL0


def graph_generator(nodeid, datestr, node_dict, v_req_edge_dict, mac_abnormality_list):
    dotfilepath = project_dir + 'pics/' + nodeid + '/' + nodeid + "_" + datestr + '.dot'
    pngfile = project_dir + 'pics/' + nodeid + '/' + nodeid + "_" + datestr + '.png'
    try:
        dotf = open(dotfilepath, 'w')
    except Exception as e:
        print("Error in visualization.graph.graph_generator " + nodeid + "_" + datestr + ": " + str(e))
        return False
    write_basic_attributes(dotf)
    node_num = len(node_dict)
    for key in node_dict:
        node = Node(key, node_dict[key])
        vnode = VisualNode(node, node_num)
        if key in mac_abnormality_list:
            write_abnormality_node(dotf, vnode)
        else:
            write_node(dotf,vnode)
    for key in v_req_edge_dict:
        edge = RequestEdge(key[0],key[1],v_req_edge_dict[key])
        vedge = VisualEdge(edge)
        write_edge(dotf, vedge)
    dotf.write('}')
    dotf.close()
    # graph pics
    command = "dot -Ksfdp " + dotfilepath + " -T png -o " + pngfile
    try:
        os.system(command)
    except Exception as e:
        print("Error in Visualization/generate_png_file " + nodeid + "_" + datestr + ": " + str(e))
        return False
    return True

def write_basic_attributes(dotf):
    dotf.write('digraph g{\n')
    dotf.write('graph [bgcolor=white, overlap=scale]\n')
    dotf.write(
        'node [fixedsize=true, shape=circle, fillcolor=gray, width=0.5, height=0.5, fontsize=5, style=filled, labelfloat=true]\n')
    dotf.write('edge [len=10,splines=line,color=white,arrowhead=vee]\n')

def write_node(dotf,vnode):
    dotf.write('\t"{0}" [width={1},height={2},fillcolor={3},fontcolor={4},fontsize={5}]\n'
                .format(vnode.node.get_macstr(), vnode.width, vnode.width, vnode.color, GRAPH_FONT_COLOR, vnode.fontsize))

def write_abnormality_node(dotf,vnode):
    dotf.write('\t"{0}" [width={1},height={2},fillcolor={3},fontcolor={4},fontsize={5}]\n'
                .format(vnode.node.get_macstr(), vnode.width, vnode.width, 'black', 'white', vnode.fontsize))

# Write edge info to dot file
def write_edge(dotf,vedge):
    dotf.write(
        '\t"{0}"->"{1}"[color={2}, style={3}];\n'.format(vedge.edge.get_src_macstr(), vedge.edge.get_dst_macstr(), vedge.color, GRAPH_LINE_TYPE))