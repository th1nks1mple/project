import os
import dpkt
import random
import binascii

def decode_macaddr(macaddr):
    macstr = macaddr.decode("utf-8")
    if(len(macstr) != 12):
        print("Error in detection/data_generator/decode_macaddr: " + str(macaddr) + " Not mac address")
        return None
    macstr_format = macstr[0:2]+":"+macstr[2:4]+":"+macstr[4:6]+":"+macstr[6:8]+":"+macstr[8:10]+":"+macstr[10:12]
    return macstr_format

def data_handle(nodeid, datestr, pcappath):
    check_edge = dict()
    cmd = "tshark -r '%s' -T fields -e eth.src -e ip.src -e eth.dst -e ip.dst > /tmp/'%s'_'%s'.txt" % (pcappath, nodeid, datestr)
    os.system(cmd)
    file_path = "/tmp/%s_%s.txt" % (nodeid, datestr)
    with open(file_path, 'r') as f:
        for line in f:
            data = line.split()
            if len(data) == 4:
                if data[1] in check_edge:
                    pass
                else:
                    check_edge[data[1]] = data[0]
                if data[3] in check_edge:
                    pass
                else:
                    check_edge[data[3]] = data[2]
    # print(check_edge)
    req_edge_dict = dict()

    with open(pcappath, 'rb') as f:
        try:
            pcap = dpkt.pcap.Reader(f)
        except Exception as e:
            print("Error in detection/data_generator" + pcappath + str(e))
            return False
        for ts, buf in pcap:
            try:
                eth = dpkt.ethernet.Ethernet(buf)
            except Exception as e:
                print("Error in detection/data_generator" + pcappath + str(e))
                continue
            if eth.type != 2054:
                continue
            try:
                arp = eth.arp
            except Exception as e:
                print("Error in detection/data_generator" + pcappath + str(e))
                continue
            mac_src = decode_macaddr(binascii.hexlify(arp.sha))
            ip_src = dpkt.socket.inet_ntoa(arp.spa)
            mac_dst = decode_macaddr(binascii.hexlify(arp.tha))
            ip_dst = dpkt.socket.inet_ntoa(arp.tpa)
            if ip_dst in check_edge:
                mac_dst = check_edge[ip_dst]
            type_arp = arp.op
            if type_arp == 1:
                if (mac_src,ip_src,mac_dst,ip_dst) in req_edge_dict:
                    req_edge_dict[(mac_src,ip_src,mac_dst,ip_dst)] += 1
                else:
                    req_edge_dict[(mac_src,ip_src,mac_dst,ip_dst)] = 1
    return req_edge_dict