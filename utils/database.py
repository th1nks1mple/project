import MySQLdb

def db_connec():
    try:
        db = MySQLdb.connect('localhost','thinksimple','password','myarpscan')
    except Exception as e:
        print("Error in utils.database.db_connect: "+str(e))
        return None
    return db
    
def create_node_group(db, node_list):
    cursor = db.cursor()
    sql = '''CREATE TABLE IF NOT EXISTS NODE (NODE_ID VARCHAR(20) NOT NULL, DATESTR_START VARCHAR(20) NOT NULL, \
            CONSTRAINT pk PRIMARY KEY (NODE_ID,DATESTR_START))'''
    res = cursor.execute(sql)
    if res != 0:
        print("Error in utils.database.create_node_group")
        return False
    for node in node_list:
        sql = '''INSERT INTO NODE (NODE_ID, DATESTR_START) VALUES ('%s', '%s')''' % (node[0], node[1])
        try:
            cursor.execute(sql)
        except Exception as e:
            print("Error in utils.database.create_node_group: "+node[0]+str(e))
            return False
    return True
def get_date_start(db, nodeid):
    cursor = db.cursor()
    sql = "SELECT DATESTR_START FROM NODE WHERE NODE_ID='%s'" % (nodeid)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_value: "+str(e))
        return False
    return cursor.fetchall()[0][0]

def create_node_table(db,nodeid):
    cursor = db.cursor()
    sql = '''CREATE TABLE IF NOT EXISTS %s (DATESTR VARCHAR(20) NOT NULL, MAC_SRC VARCHAR(20) NOT NULL, IP_SRC VARCHAR(20), \
            MAC_DST VARCHAR(20) NOT NULL, IP_DST VARCHAR(20), VALUE INTEGER NOT NULL DEFAULT 0, \
            CONSTRAINT pk PRIMARY KEY (DATESTR,MAC_SRC,IP_SRC,MAC_DST,IP_DST))''' % (nodeid)
    res = cursor.execute(sql)
    if res != 0:
        print("Error in utils.database.create_node_table "+nodeid)
        return False
    return True

def insert_value(db,nodeid,datestr,mac_src,ip_src,mac_dst,ip_dst,value):
    cursor = db.cursor()
    sql = '''INSERT INTO %s (DATESTR,MAC_SRC,IP_SRC,MAC_DST,IP_DST,VALUE) VALUES ('%s','%s','%s','%s','%s','%s')''' % (nodeid,datestr,mac_src,ip_src,mac_dst,ip_dst, value)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.insert_value: "+str(e))
        return False
    return True

def get_value(db, nodeid,datestr, mac_src):
    cursor = db.cursor()
    sql = "SELECT SUM(VALUE) FROM %s WHERE MAC_SRC='%s' and DATESTR='%s'" % (nodeid,mac_src,datestr)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_value: "+str(e))
        return False
    value =cursor.fetchall()[0][0]
    return float(value)

def get_all_mac(db, nodeid, datestr):
    cursor = db.cursor()
    sql = "SELECT DISTINCT MAC_SRC FROM %s WHERE DATESTR='%s'" % (nodeid, datestr)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_all_mac:" +str(e))
        return False
    mac_list = []
    result = cursor.fetchall()
    if (len(result) == 0):
        return []
    for row in result:
        mac_list.append(row[0])
    return mac_list
def get_v_edge_dict(db,nodeid,datestr):
    v_edge_dict = dict()
    cursor = db.cursor()
    sql = "SELECT MAC_SRC, MAC_DST, VALUE FROM %s WHERE DATESTR='%s'" % (nodeid, datestr)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_v_dege_dict:" +str(e))
        return False
    result = cursor.fetchall()
    if (len(result) == 0):
        return v_edge_dict
    for row in result:
        v_edge_dict[(row[0], row[1])] = row[2]
    return v_edge_dict

def get_all_date(db, nodeid, mac):
    cursor = db.cursor()
    sql = "SELECT DISTINCT DATESTR FROM %s WHERE MAC_SRC='%s'" % (nodeid, mac)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_all_date:" +str(e))
        return False
    date_list = []
    result = cursor.fetchall()
    if (len(result) == 0):
        return []
    for row in result:
        date_list.append(row[0])
    return date_list

def get_data_detect(db, nodeid, datestr,mac):
    data = dict()
    date_list = get_all_date(db, nodeid, mac)
    for date in date_list:
        cursor = db.cursor()
        sql = "SELECT SUM(VALUE) FROM %s WHERE DATESTR='%s' and MAC_SRC='%s'" % (nodeid, date,mac)
        try:
            cursor.execute(sql)
        except Exception as e:
            print("Error in utils.database.get_data_detect:" +str(e))
            return False
        value = cursor.fetchall()[0][0]
        data[date] = float(value)
    return data

def create_abnormality_table(db):
    cursor = db.cursor()
    sql = '''CREATE TABLE IF NOT EXISTS ABNORMALITY (NODE_ID VARCHAR(10) NOT NULL, MAC VARCHAR(20) NOT NULL, \
            DATESTR VARCHAR(10) NOT NULL, CONSTRAINT pk PRIMARY KEY (NODE_ID, MAC, DATESTR))'''
    res = cursor.execute(sql)
    if(res != 0):
        print("Error in utils.database.create_abnormality_table.")
        return False
    return True

def insert_abnormality(db,nodeid,datestr,mac):
    cursor = db.cursor()
    sql = '''INSERT INTO ABNORMALITY (NODE_ID, MAC, DATESTR) VALUES ('%s','%s','%s')''' % (nodeid, mac, datestr)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.insert_abnormality:" +str(e))
        return False
    return True

def get_abnormality(db,nodeid,datestr):
    cursor = db.cursor()
    sql = "SELECT MAC FROM ABNORMALITY WHERE NODE_ID='%s' and DATESTR='%s'" % (nodeid, datestr)
    try:
        cursor.execute(sql)
    except Exception as e:
        print("Error in utils.database.get_abnormality:" +str(e))
        return False
    mac_list = []
    result = cursor.fetchall()
    if (len(result) == 0):
        return []
    for row in result:
        mac_list.append(row[0])
    return mac_list

# if __name__ == "__main__":
#     db = db_connec()
#     insert_value(db,'n061','20191218','03:dw:50:c4:ae:65','192.168.34.24','03:dw:50:c5:ae:65','192.169.34.76',45)
#     insert_value(db,'n061','20191218','03:dw:50:c4:ae:65','192.168.34.24','03:dw:50:c5:ae:66','192.169.34.76',45)
#     value = get_value(db,'n061','20191218','03:dw:50:c4:ae:65',)
#     print(value)
#     create_abnormality_table(db)
#     insert_abnormality(db,'n061','20191218','03:dw:50:c4:ae:65')
#     insert_abnormality(db,'n061','20191218','03:dw:50:c5:ae:65')
#     list_mac = get_abnormality(db,'n061','20191218')
#     print(list_mac)