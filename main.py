import argparse
import os
import glob
import sys
from detection import abnormality_detecttion
from utils import (data_generator, database)
from visualization import graph

project_dir = os.path.dirname(os.path.realpath(__file__))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Do something magic (-_-)")
    parser.add_argument("nodeid", metavar='NODE',help="nodeid you want to process")
    parser.add_argument("datestr", metavar='DATE', help="date you want to process Format= yyyymmdd")
    args = parser.parse_args()
    
    db = database.db_connec()
    res = glob.glob(project_dir+'data/'+args.nodeid+'/pcap/'+args.nodeid+'_'+args.datestr+'*.pcap')
    pcappath = res[0]
    # get data
    req_edge_dict = data_generator.data_handle(args.nodeid,args.datestr, pcappath)

    for keys in req_edge_dict:
        database.insert_value(db,args.nodeid,args.datestr,keys[0],keys[1],keys[2],keys[3],req_edge_dict[keys])
    # detect
    mac_list = database.get_all_mac(db, args.nodeid,args.datestr)
    for mac in mac_list:
        data = database.get_data_detect(db, args.nodeid,args.datestr,mac)
        if(len(data) <= 3): # mac-new 
            continue
        if((abnormality_detecttion.detector(data, args.datestr)) == True):
            database.insert_abnormality(db, args.nodeid,args.datestr,mac)
    # graph
    node_dict = dict()
    for mac in mac_list:
        node_dict[mac] = database.get_value(db, args.nodeid,args.datestr,mac)
    v_req_edge_dict = dict()
    v_req_edge_dict = database.get_v_edge_dict(db, args.nodeid, args.datestr)
    mac_abnormality_list = database.get_abnormality(db, args.nodeid,args.datestr)
    print(mac_abnormality_list)
    graph.graph_generator(args.nodeid, args.datestr, node_dict, v_req_edge_dict, mac_abnormality_list)
    db.commit()
    db.close()